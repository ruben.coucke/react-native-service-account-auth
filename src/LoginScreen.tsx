import React, {useState} from 'react';
import {Button, SafeAreaView, StyleSheet, Text, View} from 'react-native';
import {clearToken, getToken} from './auth';
import instance from './api';
import api from './api';

const makeMockRequest = () => {
    api.get('AN_ENDPOINT');
}

const LoginScreen = () => {
    const [token, setToken] = useState('');

    return (
        <SafeAreaView style={styles.body}>
            <View>
                <View>
                    {token ?
                        <>
                            <Button
                                onPress={() => {
                                    clearToken();
                                    setToken('');
                                }}
                                title="Log out"
                                color="red"/>
                            <Button title={'Make mock request'} onPress={makeMockRequest}/>
                        </>
                        :
                        <>
                            <Button title={'Log in'} onPress={() => getToken(setToken)}/>
                            <Text>You are currently logged out</Text>
                        </>
                    }
                </View>
            </View>
        </SafeAreaView>
    );
}

const styles = StyleSheet.create({
    body: {
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1
    }
});

export default LoginScreen;
