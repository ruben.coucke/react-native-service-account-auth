import axios from 'axios';
import AsyncStorage from '@react-native-community/async-storage';
import {getToken, STORAGE_TOKEN_KEY} from './auth';

const api = axios.create({
    baseURL: 'http://google.com'
});

api.interceptors.request.use(
    async (config) => {
        const token = await AsyncStorage.getItem(STORAGE_TOKEN_KEY)
        if (token) {
            config.headers.Authorization = `Bearer ${token}`;
        }
        return config;
    },
    (err) => {
        return Promise.reject(err);
    }
);

api.interceptors.response.use(response => response, error => {
    if (error.config && error.response && error.response.status === 401) {
        const token = getToken();
        error.config.headers.Authorization = `Bearer ${token}`;
        return api.request(error.config);
    }
    return Promise.reject(error);
});

export default api;
