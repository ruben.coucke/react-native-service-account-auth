import {KJUR} from 'jsrsasign';
import AsyncStorage from '@react-native-community/async-storage';
import PRIVATE_KEY from './private_key.json';

const TARGET_AUD = `https://rbfa-aca-esp-svc-dot-rbfa-dig-int-dev.appspot.com`;

let refreshTokenInterval: number;

export const STORAGE_TOKEN_KEY = 'token';

export const clearToken = () => {
    AsyncStorage.removeItem(STORAGE_TOKEN_KEY).catch(console.log);
}

const expirationTime = 3600;

export const getToken = (signinSuccessfulCallback: (token: string) => void = () => {}): string => {
    const now = KJUR.jws.IntDate.getNow();

    const header = {
        alg: 'RS256',
        typ: 'JWT'
    };
    const payload = {
        iat: now,
        exp: now + expirationTime,
        iss: PRIVATE_KEY.client_email,
        target_audience: TARGET_AUD,
        aud: TARGET_AUD
    };

    const token = KJUR.jws.JWS.sign('RS256', header, payload, PRIVATE_KEY.private_key);

    if (refreshTokenInterval) {
        clearTimeout(refreshTokenInterval);
    }
    refreshTokenInterval = setTimeout(() => getToken(signinSuccessfulCallback), expirationTime * 1000);

    AsyncStorage.setItem(STORAGE_TOKEN_KEY, token).then(() => {
        signinSuccessfulCallback(token);
        console.log(token);
    });
    return token;
};
